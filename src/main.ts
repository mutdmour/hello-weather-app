import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { createI18n } from 'vue-i18n';
import translations from './i18n';

import App from './App.vue';
import router from './router';
import Toast, { POSITION } from 'vue-toastification';
import type { PluginOptions } from 'vue-toastification';

import 'element-plus/theme-chalk/dark/css-vars.css';
import 'vue-toastification/dist/index.css';
import './assets/main.css';
import './assets/base.css';

const app = createApp(App);

app.use(createPinia());
app.use(router);
app.use(
	createI18n({
		legacy: false,
		locale: 'en',
		fallbackLocale: 'en',
		messages: translations,
	})
);
const toastOptions: PluginOptions = {
	newestOnTop: true,
	position: POSITION.BOTTOM_RIGHT,
	maxToasts: 3,
};
app.use(Toast, toastOptions);

app.mount('#app');
