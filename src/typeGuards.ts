import type {
	WeatherQueryByCoord,
	WeatherQuerySearch,
	LocationWeatherInfo,
	LocationWeatherInfoLoaded,
	LocationWeatherInfoErrored,
	LocationWeatherInfoLoading,
	ApiError,
	UnitType,
	Theme,
	SearchQuery,
	Coordinate,
} from 'Interface';

export function isSearchQuery(query: unknown): query is SearchQuery {
	return (
		!!query &&
		typeof query === 'object' &&
		Object.prototype.hasOwnProperty.call(query, 'q')
	);
}

export function isCoord(query: unknown): query is Coordinate {
	return (
		!!query &&
		typeof query === 'object' &&
		Object.prototype.hasOwnProperty.call(query, 'lon') &&
		Object.prototype.hasOwnProperty.call(query, 'lat')
	);
}

export function isWeatherQuerySearch(
	query: unknown
): query is WeatherQuerySearch {
	return (
		!!query &&
		typeof query === 'object' &&
		Object.prototype.hasOwnProperty.call(query, 'q') &&
		Object.prototype.hasOwnProperty.call(query, 'units')
	);
}

export function isWeatherQueryByCoord(
	query: unknown
): query is WeatherQueryByCoord {
	return (
		!!query &&
		typeof query === 'object' &&
		Object.prototype.hasOwnProperty.call(query, 'lon') &&
		Object.prototype.hasOwnProperty.call(query, 'lat') &&
		Object.prototype.hasOwnProperty.call(query, 'units')
	);
}

export function isLocationWeatherInfoLoaded(
	info: LocationWeatherInfo
): info is LocationWeatherInfoLoaded {
	return (
		info.loading === false &&
		info.error === false &&
		Object.prototype.hasOwnProperty.call(info, 'response')
	);
}

export function isLocationWeatherInfoErrored(
	info: LocationWeatherInfo
): info is LocationWeatherInfoErrored {
	return info.error;
}

export function isLocationWeatherInfoLoading(
	info: LocationWeatherInfo
): info is LocationWeatherInfoLoading {
	return info.loading;
}

export function isOpenApiWeatherError(info: unknown): info is ApiError {
	return !!(
		typeof info === 'object' &&
		info &&
		Object.prototype.hasOwnProperty.call(info, 'message') &&
		Object.prototype.hasOwnProperty.call(info, 'cod')
	);
}

export function isUnitType(value: string | null): value is UnitType {
	return !!value && ['imperial', 'standard', 'metric'].includes(value);
}

export function isTheme(value: string | null): value is Theme {
	return ['light', 'dark', null].includes(value);
}
