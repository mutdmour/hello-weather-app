import { isOpenApiWeatherError } from '../typeGuards';
import type { LocationsResponse, LocationsQuery, SearchQuery } from 'Interface';
import {
	LCOAL_STORAGE_LOCATIONS,
	DEFAULT_LOCATION_QUERIES,
} from '../constants';

const DELIMITER = '|';

export function getSavedLocations(): SearchQuery[] {
	const locationsStr = localStorage.getItem(LCOAL_STORAGE_LOCATIONS);
	if (locationsStr === null) {
		return DEFAULT_LOCATION_QUERIES;
	}
	if (!locationsStr) {
		return [];
	}

	return locationsStr.split(DELIMITER).map((q: string) => {
		return {
			q,
		};
	});
}

export function saveLocations(queries: SearchQuery[]) {
	const value = queries.map((query) => query.q).join(DELIMITER);
	localStorage.setItem(LCOAL_STORAGE_LOCATIONS, value);
}

export async function fetchLocations(
	query: LocationsQuery,
	settings: {
		apiEndpointKey: string;
		apiEndpoint: string;
	}
): Promise<LocationsResponse> {
	let url = `${settings.apiEndpoint}/geo/1.0/direct?appid=${settings.apiEndpointKey}&q=${query.q}`;
	url = query.limit ? `${url}&limit=${query.limit}` : url;
	const response = await fetch(url);

	if (!response.ok) {
		const error = (await response.json()) as unknown;
		if (isOpenApiWeatherError(error)) {
			throw Error(`${error.message}`);
		}

		throw Error(`${response.statusText || response.status}`);
	}

	try {
		return await response.json();
	} catch (e) {
		throw Error('Invalid response');
	}
}
