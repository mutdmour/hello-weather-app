import { isOpenApiWeatherError } from '../typeGuards';
import type {
	WeatherResponse,
	WeatherQuerySearch,
	WeatherQueryByCoord,
} from '../../Interface';

export async function fetchWeatherByQuery(
	query: WeatherQuerySearch,
	settings: {
		apiEndpointKey: string;
		apiEndpoint: string;
	}
): Promise<WeatherResponse> {
	const units = query.units ?? 'metric';
	// todo use URL constructor
	const response = await fetch(
		`${settings.apiEndpoint}/data/2.5/weather?q=${query.q}&appid=${settings.apiEndpointKey}&units=${units}`
	);

	if (!response.ok) {
		const error = (await response.json()) as unknown;
		if (isOpenApiWeatherError(error)) {
			throw Error(`${error.message}`);
		}

		throw Error(`${response.statusText || response.status}`);
	}

	try {
		return await response.json();
	} catch (e) {
		// todo i18n
		throw Error('Invalid response');
	}
}

export async function fetchWeatherByCoord(
	query: WeatherQueryByCoord,
	settings: {
		apiEndpointKey: string;
		apiEndpoint: string;
	}
): Promise<WeatherResponse> {
	const units = query.units ?? 'metric';
	const response = await fetch(
		`${settings.apiEndpoint}/data/2.5/weather?lat=${query.lat}&lon=${query.lon}&appid=${settings.apiEndpointKey}&units=${units}`
	);

	if (!response.ok) {
		const error = (await response.json()) as unknown;
		if (isOpenApiWeatherError(error)) {
			throw Error(`${error.message}`);
		}

		throw Error(`${response.statusText || response.status}`);
	}

	try {
		return await response.json();
	} catch (e) {
		throw Error('Invalid response');
	}
}
