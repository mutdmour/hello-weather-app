import {
	ENV_VAR_API_ENDPOINT,
	ENV_VAR_API_KEY,
	LCOAL_STORAGE_UNITS,
	LOCAL_STORAGE_THEME,
} from '../constants';
import { isTheme, isUnitType } from '../typeGuards';
import type { Theme, UnitType } from 'Interface';

const apiEndpoint = import.meta.env[ENV_VAR_API_ENDPOINT] || '';
const apiEndpointKey = import.meta.env[ENV_VAR_API_KEY] || '';

const savedTheme = window.localStorage.getItem(LOCAL_STORAGE_THEME);
let theme = isTheme(savedTheme) ? savedTheme : null;
if (!theme) {
	// default to browser defaults
	if (window?.matchMedia?.('(prefers-color-scheme: dark)').matches) {
		theme = 'dark';
	} else {
		theme = 'light';
	}
}

const savedUnits = window.localStorage.getItem(LCOAL_STORAGE_UNITS);
const units: UnitType = isUnitType(savedUnits) ? savedUnits : 'metric';

export function initSettings(): {
	apiEndpoint: string;
	apiEndpointKey: string;
	units: UnitType;
	theme: Theme;
} {
	return {
		apiEndpoint,
		apiEndpointKey,
		units,
		theme: theme || 'light',
	};
}

export function askForUserLocation(): Promise<GeolocationPosition> {
	return new Promise((resolve, reject) => {
		if (window.navigator.geolocation) {
			window.navigator.geolocation.getCurrentPosition(resolve, reject);
		}
	});
}

export function setTheme(theme: Theme) {
	if (theme === null) {
		window.localStorage.removeItem(LOCAL_STORAGE_THEME);
	} else {
		window.localStorage.setItem(LOCAL_STORAGE_THEME, theme);
	}
}

export function setUnits(units: UnitType) {
	window.localStorage.setItem(LCOAL_STORAGE_UNITS, units);
}
