import { describe, it, expect } from 'vitest';

import { shallowMount } from '@vue/test-utils';
import EllipsisLoader from '../EllipsisLoader.vue';

describe('EllipsisLoader', () => {
	it('renders ellipsis loader', () => {
		const wrapper = shallowMount(EllipsisLoader);
		expect(wrapper.html()).toMatchSnapshot();
	});
});
