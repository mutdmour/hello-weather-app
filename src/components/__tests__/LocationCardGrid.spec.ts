import { describe, it, expect, vi } from 'vitest';
import { createTestingPinia } from '@pinia/testing';
import { shallowMount } from '@vue/test-utils';
import LocationCardGrid, {
	type LocationCardGridProps,
} from '../LocationCardGrid.vue';

describe('LocationCardGrid', () => {
	it('renders grid', () => {
		const props: LocationCardGridProps = {
			locations: [
				{
					id: 'loading',
					data: {
						loading: true,
					},
					current: false,
				},
				{
					id: 'card',
					data: {
						loading: false,
						name: 'name test',
						url: '/url',
						temp: 12.18,
						units: 'metric',
					},
					current: false,
				},
			],
		};
		const wrapper = shallowMount(LocationCardGrid, {
			props,
			global: {
				plugins: [
					createTestingPinia({
						createSpy: vi.fn,
					}),
				],
			},
		});
		expect(wrapper.html()).toMatchSnapshot();
	});
});
