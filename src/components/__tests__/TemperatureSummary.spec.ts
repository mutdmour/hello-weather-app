import { describe, it, expect } from 'vitest';

import { shallowMount } from '@vue/test-utils';
import TemperatureSummary, {
	type TemperatureSummaryProps,
} from '../TemperatureSummary.vue';

describe('TemperatureSummary', () => {
	it('renders temperature details', () => {
		const props: TemperatureSummaryProps = {
			temp: 12.12,
			minTemp: 13.3,
			maxTemp: 15.5,
			units: 'metric',
		};
		const wrapper = shallowMount(TemperatureSummary, { props });
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('renders temperature details with condition', () => {
		const props: TemperatureSummaryProps = {
			temp: 12.12,
			minTemp: 13.3,
			maxTemp: 15.5,
			condition: 'Sunny',
			units: 'metric',
		};
		const wrapper = shallowMount(TemperatureSummary, { props });
		expect(wrapper.html()).toMatchSnapshot();
	});
});
