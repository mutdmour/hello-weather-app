import { describe, it, expect } from 'vitest';

import { shallowMount } from '@vue/test-utils';
import WeatherSummary, {
	type WeatherSummaryProps,
} from '../WeatherSummary.vue';

describe('WeatherSummary', () => {
	it('renders weather summary', () => {
		const props: WeatherSummaryProps = {
			response: {
				coord: {
					lon: 13.4105,
					lat: 52.5244,
				},
				weather: [
					{
						id: 800,
						main: 'Clear',
						description: 'clear sky',
						icon: '01n',
					},
				],
				base: 'stations',
				main: {
					temp: 280,
					feels_like: 276.74,
					temp_min: 278.2,
					temp_max: 281.47,
					pressure: 1002,
					humidity: 87,
				},
				visibility: 10000,
				wind: {
					speed: 5.14,
					deg: 150,
				},
				clouds: {
					all: 0,
				},
				dt: 1667768901,
				sys: {
					type: 2,
					id: 2011538,
					country: 'DE',
					sunrise: 1667715086,
					sunset: 1667748511,
				},
				timezone: 3600,
				id: 2950159,
				name: 'Berlin',
				cod: 200,
			},
		};
		const wrapper = shallowMount(WeatherSummary, { props });
		expect(wrapper.html()).toMatchSnapshot();
	});
});
