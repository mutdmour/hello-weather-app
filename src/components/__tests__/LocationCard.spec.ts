import { describe, it, expect } from 'vitest';

import { shallowMount } from '@vue/test-utils';
import LocationCard from '../LocationCard.vue';
import type { LocationCardProps } from '../LocationCard.vue';

describe('LocationCard', () => {
	it('renders location card loading', () => {
		const props: LocationCardProps = {
			id: 'id',
			data: {
				loading: true,
			},
			current: false,
		};
		const wrapper = shallowMount(LocationCard, { props });
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('renders location card with data', () => {
		const props: LocationCardProps = {
			id: 'id',
			data: {
				loading: false,
				name: 'name test',
				url: '/url',
				temp: 12.18,
				units: 'metric',
			},
			current: false,
		};
		const wrapper = shallowMount(LocationCard, { props });
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('renders location card with current location', () => {
		const props: LocationCardProps = {
			id: 'id',
			data: {
				loading: false,
				name: 'name test',
				url: '/url',
				temp: 12.18,
				units: 'metric',
			},
			current: true,
		};
		const wrapper = shallowMount(LocationCard, { props });
		expect(wrapper.html()).toMatchSnapshot();
	});
});
