import { describe, it, expect } from 'vitest';

import { shallowMount } from '@vue/test-utils';
import FAIcon from '../FAIcon.vue';

describe('FAIcon', () => {
	it('renders arrow back with default size', () => {
		const wrapper = shallowMount(FAIcon, { props: { name: 'arrow-left' } });
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('renders icon small', () => {
		const wrapper = shallowMount(FAIcon, {
			props: { name: 'arrow-left', size: 'small' },
		});
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('renders icon medium', () => {
		const wrapper = shallowMount(FAIcon, {
			props: { name: 'arrow-left', size: 'medium' },
		});
		expect(wrapper.html()).toMatchSnapshot();
	});

	it('renders icon large', () => {
		const wrapper = shallowMount(FAIcon, {
			props: { name: 'arrow-left', size: 'large' },
		});
		expect(wrapper.html()).toMatchSnapshot();
	});
});
