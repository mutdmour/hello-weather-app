import { defineStore } from 'pinia';
import { fetchWeatherByQuery, fetchWeatherByCoord } from '../api/weather';
import type {
	WeatherStore,
	WeatherQuerySearch,
	WeatherQueryByCoord,
	LocationWeatherInfo,
	WeatherQuery,
	SearchQuery,
	Coordinate,
	WeatherResponse,
} from 'Interface';
import {
	isCoord,
	isSearchQuery,
	isLocationWeatherInfoLoaded,
	isWeatherQueryByCoord,
	isWeatherQuerySearch,
} from '../typeGuards';
import { useSettingsStore } from '../stores/settings';

export function getQueryKey(
	query: WeatherQuery | SearchQuery | Coordinate | string | unknown
) {
	const units = useSettingsStore().units;
	if (typeof query === 'string') {
		return `${units}:${query}`;
	}

	if (isWeatherQueryByCoord(query) || isCoord(query)) {
		return `${units}:${query.lat},${query.lon}`;
	} else if (isWeatherQuerySearch(query) || isSearchQuery(query)) {
		return `${units}:${query.q}`;
	}
}

export const useWeatherStore = defineStore('weather', {
	state: (): WeatherStore => ({ locations: {} }),
	getters: {
		allLocations(): LocationWeatherInfo[] {
			return Object.values(this.locations);
		},
		getWeatherByQuery() {
			return (
				query: SearchQuery | Coordinate
			): LocationWeatherInfo | undefined => {
				const key = getQueryKey(query);
				if (key) {
					return this.locations[key];
				}
			};
		},
		getWeatherByKey() {
			return (key: string): LocationWeatherInfo | undefined => {
				return this.locations[key];
			};
		},
	},
	actions: {
		addLocationLoading(locationId: string) {
			this.locations[locationId] = {
				id: locationId,
				loading: true,
			};
		},
		removeLocation(location: string) {
			delete this.locations[location];
		},
		async getWeatherCondition(
			params: Coordinate | SearchQuery,
			options?: { key: string }
		): Promise<LocationWeatherInfo> {
			const settingsStore = useSettingsStore();
			const units = settingsStore.units;
			const query: WeatherQueryByCoord | WeatherQuerySearch = {
				...params,
				units,
			};

			const key = options?.key ?? getQueryKey(query);
			if (!key) {
				throw Error('Unknown');
			}
			try {
				if (
					this.locations[key] &&
					isLocationWeatherInfoLoaded(this.locations[key])
				) {
					return this.locations[key];
				}
				this.locations[key] = {
					id: key,
					query,
					loading: true,
					error: false,
				};
				let response: WeatherResponse;
				if (isWeatherQueryByCoord(query)) {
					response = await fetchWeatherByCoord(query, {
						apiEndpoint: settingsStore.apiEndpoint,
						apiEndpointKey: settingsStore.apiEndpointKey,
					});
				} else {
					response = await fetchWeatherByQuery(query, {
						apiEndpoint: settingsStore.apiEndpoint,
						apiEndpointKey: settingsStore.apiEndpointKey,
					});
				}
				this.locations[key] = {
					...this.locations[key],
					response,
					loading: false,
				};

				return this.locations[key];
			} catch (e: unknown) {
				this.locations[key].error = true;
				this.locations[key].loading = false;
				throw e;
			}
		},
	},
});
