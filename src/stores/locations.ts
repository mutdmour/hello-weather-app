import { defineStore } from 'pinia';
import type { LocationsInfo, LocationsStore, SearchQuery } from 'Interface';
import {
	fetchLocations,
	getSavedLocations,
	saveLocations,
} from '../api/locations';
import { useSettingsStore } from './settings';

const initalLocations = getSavedLocations();

export const useLocationsStore = defineStore('locations', {
	state: (): LocationsStore => ({ searches: {}, queries: initalLocations }),
	getters: {
		getLocationBySearch() {
			return (query: string): LocationsInfo | undefined => {
				return this.searches[query];
			};
		},
	},
	actions: {
		addQuery(toAdd: SearchQuery) {
			const duplicate = this.queries.find((query) => query.q === toAdd.q);
			if (duplicate) {
				return;
			}

			this.queries = [toAdd, ...this.queries];
			saveLocations(this.queries);
		},
		removeQuery(q: string) {
			this.queries = this.queries.filter((query) => query.q !== q);
			saveLocations(this.queries);
		},
		async searchLocations(query: string): Promise<LocationsInfo> {
			if (this.searches[query]) {
				return this.searches[query];
			}

			this.searches[query] = {
				loading: true,
				error: false,
			};

			try {
				const settingsStore = useSettingsStore();
				this.searches[query].response = await fetchLocations(
					{
						q: query,
						limit: 10,
					},
					{
						apiEndpoint: settingsStore.apiEndpoint,
						apiEndpointKey: settingsStore.apiEndpointKey,
					}
				);
				this.searches[query].loading = false;

				return this.searches[query];
			} catch (e) {
				this.searches[query].loading = false;
				this.searches[query].error = true;

				throw e;
			}
		},
	},
});
