import { defineStore } from 'pinia';
import type { SettingsStore, Theme, UnitType } from 'Interface';
import {
	askForUserLocation,
	initSettings,
	setTheme,
	setUnits,
} from '../api/settings';

const settings = initSettings();

export const useSettingsStore = defineStore('settings', {
	state: (): SettingsStore => ({
		apiEndpoint: settings.apiEndpoint,
		apiEndpointKey: settings.apiEndpointKey,
		userLocation: null,
		theme: settings.theme,
		units: settings.units,
	}),
	actions: {
		setTheme(theme: Theme) {
			this.theme = theme;
			setTheme(theme);
		},
		setUnits(units: UnitType) {
			this.units = units;
			setUnits(units);
		},
		async getUserLocation() {
			if (this.userLocation) {
				return this.userLocation;
			}

			const geoLocation = await askForUserLocation();
			this.userLocation = {
				lat: geoLocation.coords.latitude,
				lon: geoLocation.coords.longitude,
			};
			return this.userLocation;
		},
	},
});
