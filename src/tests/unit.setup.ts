import { config } from '@vue/test-utils';
import { createI18n } from 'vue-i18n';

const i18n = createI18n({
	legacy: false,
});

config.global.plugins = [i18n];

config.global.mocks = {
	$t: (key: string, params: Object) =>
		`t(${key}, ${JSON.stringify(params || {})}`,
};
