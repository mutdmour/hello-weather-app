import type { SearchQuery } from 'Interface';

export const DEFAULT_LOCATION_QUERIES: SearchQuery[] = [
	{
		q: 'Berlin,DE',
	},
	{
		q: 'London,England,GB',
	},
];

export const CURRENT_LOCATION_KEY = 'current-location';

export const ENV_VAR_API_ENDPOINT = 'VITE_APP_WEATHER_API_ENDPOINT';
export const ENV_VAR_API_KEY = 'VITE_APP_WEATHER_API_APP_KEY';

export const LOCAL_STORAGE_THEME = 'hw-theme';
export const LCOAL_STORAGE_UNITS = 'hw-units';
export const LCOAL_STORAGE_LOCATIONS = 'hw-locations';
