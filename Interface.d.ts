export type Meters = number;
export type MetersPerSecond = number;
export type MilesPerHour = number;
export type Speed = MilesPerHour | MetersPerSecond;
export type Degree = number;
export type Percentage = number;
export type PressueUnit = number;
export type Kelvin = number;
export type Celsius = number;
export type Fahrenheit = number;
export type Temperature = Kelvin | Celsius | Fahrenheit;
export type Milimeter = number;
export type TimeUTC = number;
export type Seconds = number;
export type InternalApiProperty = unknown;
export type UnitType = 'imperial' | 'standard' | 'metric';
export type Theme = 'light' | 'dark';

export interface Coordinate {
	lon: number;
	lat: number;
}

export interface WeatherCondition {
	id: number;
	main: string;
	description: string;
	icon: string;
}

export interface TemperatureInfo {
	temp: Temperature;
	feels_like: Temperature;
	temp_min: Temperature;
	temp_max: Temperature;
	pressure: PressueUnit;
	humidity: Percentage;
	sea_level?: PressueUnit;
	grnd_level?: PressueUnit;
}

export interface WindCondition {
	speed: Speed;
	deg: Degree;
	gust?: Speed;
}

export interface CloudsCondition {
	all: Percentage;
}

export interface VolumeCondition {
	'1h': Milimeter;
	'3h'?: Milimeter;
}

export interface SystemLocationInfo {
	type?: InternalApiProperty;
	id?: InternalApiProperty;
	message?: InternalApiProperty;
	country: string;
	sunrise?: TimeUTC;
	sunset?: TimeUTC;
}

export interface WeatherQueryBase {
	units: UnitType;
	lang?: string;
	exclude?: ('current' | 'minutely' | 'hourly' | 'daily' | 'alerts')[];
}

export interface SearchQuery {
	q: string;
}

export interface WeatherQuerySearch extends WeatherQueryBase, SearchQuery {}

export interface WeatherQueryByCoord extends WeatherQueryBase, Coordinate {}

export type WeatherQuery = WeatherQueryByCoord | WeatherQuerySearch;

export interface WeatherResponse {
	base?: InternalApiProperty;
	coord: Coordinate;
	weather: WeatherCondition[];
	main: TemperatureInfo;
	visibility: Meters;
	wind?: WindCondition;
	clouds?: CloudsCondition;
	rain?: VolumeCondition;
	snow?: VolumeCondition;
	dt: TimeUTC;
	sys?: SystemLocationInfo;
	timezone: Seconds;
	name: string;
	id: number;
	cod?: InternalApiProperty;
}

interface LocationWeatherInfoBase {
	id: string;
	query: WeatherQuery;
}

interface LocationWeatherInfoLoading extends LocationWeatherInfoBase {
	loading: true;
	error: false;
}

interface LocationWeatherInfoLoaded extends LocationWeatherInfoBase {
	loading: false;
	error: false;
	response: WeatherResponse;
}

interface LocationWeatherInfoErrored extends LocationWeatherInfoBase {
	loading: false;
	error: true;
}

export type LocationWeatherInfo =
	| LocationWeatherInfoLoaded
	| LocationWeatherInfoLoading
	| LocationWeatherInfoErrored;

export interface LocationQueryBase {
	q: string;
}

export interface LocationsQuery extends LocationQueryBase {
	limit?: number;
}

export interface LocationDetails {
	name: string;
	local_names: {
		[locale: string]: string;
	};
	lat: number;
	lon: number;
	country: string;
	state?: string;
}

export type LocationsResponse = LocationDetails[];

export interface LocationsInfoBase {
	query: LocationsQuery;
}

interface LocationsInfoLoading extends LocationWeatherInfoBase {
	loading: true;
	error: false;
}

interface LocationsInfoLoaded extends LocationsInfoBase {
	loading: false;
	error: false;
	response: LocationsResponse;
}

interface LocationsInfoErrored extends LocationsInfoBase {
	loading: false;
	error: true;
}

export type LocationsInfo =
	| LocationsInfoLoaded
	| LocationsInfoLoading
	| LocationsInfoErrored;

export interface LocationsStore {
	searches: Ref<{
		[search: string]: LocationsInfo;
	}>;
	queries: SearchQuery[];
}

export interface WeatherStore {
	locations: Ref<{
		[location: string]: LocationWeatherInfo;
	}>;
}

export interface SettingsStore {
	apiEndpoint: sring;
	apiEndpointKey: string;
	userLocation: Coordinate | null;
	theme: Theme;
	units: UnitType;
}

export interface ApiError {
	cod: number;
	message: string;
}
