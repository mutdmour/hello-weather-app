# HelloWeatherApp 

## DEMO
[You can view app here](https://mutdmour.gitlab.io/hello-weather-app/)

## Approach
1. Types as documentation..
2. ESLint for code linting with default Vue rules
3. Vue as framework.. Pinia as data store..
4. vue-i18n for localization
5. Cypress for E2E tests
6. Vitest for unit tests, using snapshots and shallow mounts
7. CSS modules 
8. Human readable class names for components for user overrides and automated tests
9. CSS variables for themes
10. local storage for personalization (units, themes..)
11. FontAwesome for icons cached behind CDN
12. Husky and conventional commits for Git log
13. Prettier to style the code based on what I am used to (like tabs even though I prefer spaces)
14. Default page layout for all views
15. .env file to configure Weather API
16. Toast notifications for errors
17. Error views and Warning banner if access to user location is blocked
18. Element UI Framework imported dynamically. Used for Select component
20. Gitlab for CI/CD and Pages for demo

## To improve
1. Increase coverage for unit (store and api layers espicially) and e2e tests
2. Validate API responses using types for maximum type safety
3. Storybook build to demo component
4. CSS or SCSS tokens to standarize css properties (font size, font weight, spacing tokens...)
5. API endpoint to retrieve multiple locations
6. Support translation params in component tests with i18n translations in scripts
7. Docker build
8. Test old browsers
9. Add types to translation function based on translation keys
10. Update all css files to scss, clean up css
11. Increase color contrast ratio (based on Lighthouse accessibility test)
12. Change location endpoint from query to path like /current or /location/london
13. TailwindCSS for CSS
14. SEO optimizations
15. Handle unimportant warnings when running unit tests
16. Translate temp between units without making API requests
17. Add SSR support
18. .env files should not be committed to Git
19. On mobile, requires two clicks to open location (shows remove button first).. need to adjust design to avoid this
20. There's a lot of content shifting in the beginning because I chose to make the plus a card.. 
21. Use fixtures for E2E tests instead
22. Improve mobile UI/UX
21. Figure out why Safari on my iPhone is not requesting location

## To setup

```sh
yarn install
```
For demo purposes for this project and since these credentials are not critical and free, I have decided to commit the env variables to Git 😨

### To develop

```sh
yarn run dev
```

### To build and run in production

```sh
yarn run build;
yarn run start;
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
yarn run test:unit
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/) for development

```sh
yarn run test:e2e:dev
```

### To test the production build E2E

```sh
yarn run build
yarn run test:e2e
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn run lint
```
