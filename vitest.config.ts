import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [vue()],
	// @ts-ignore
	test: {
		setupFiles: ['src/tests/unit.setup.ts'],
		css: {
			modules: {
				classNameStrategy: 'non-scoped',
			},
		},
	},
});
