export const MOCK_BERLIN_RESPONSE = {
	coord: { lon: 13.4105, lat: 52.5244 },
	weather: [
		{ id: 800, main: 'Clear', description: 'clear sky', icon: '01n' },
	],
	base: 'stations',
	main: {
		temp: 12.16,
		feels_like: 11.39,
		temp_min: 10.55,
		temp_max: 13.32,
		pressure: 1000,
		humidity: 75,
	},
	visibility: 10000,
	wind: { speed: 3.6, deg: 170 },
	clouds: { all: 0 },
	dt: 1667942582,
	sys: {
		type: 2,
		id: 2011538,
		country: 'DE',
		sunrise: 1667888108,
		sunset: 1667921105,
	},
	timezone: 3600,
	id: 2950159,
	name: 'Berlin',
	cod: 200,
};

export const MOCK_LONDON_RESPONSE = {
	coord: { lon: -0.1257, lat: 51.5085 },
	weather: [
		{ id: 803, main: 'Clouds', description: 'broken clouds', icon: '04n' },
	],
	base: 'stations',
	main: {
		temp: 10.33,
		feels_like: 9.74,
		temp_min: 9.14,
		temp_max: 11.16,
		pressure: 1002,
		humidity: 89,
	},
	visibility: 10000,
	wind: { speed: 4.63, deg: 230 },
	clouds: { all: 56 },
	dt: 1667770899,
	sys: {
		type: 2,
		id: 2075535,
		country: 'GB',
		sunrise: 1667718151,
		sunset: 1667751943,
	},
	timezone: 0,
	id: 2643743,
	name: 'London',
	cod: 200,
};

export const MOCK_KOSICE_RESPONSE = {
	coord: { lon: 21.2578, lat: 48.721 },
	weather: [
		{ id: 800, main: 'Clear', description: 'clear sky', icon: '01n' },
	],
	base: 'stations',
	main: {
		temp: 7.71,
		feels_like: 6.99,
		temp_min: 6.84,
		temp_max: 7.76,
		pressure: 1018,
		humidity: 92,
	},
	visibility: 10000,
	wind: { speed: 1.54, deg: 260 },
	clouds: { all: 0 },
	dt: 1667771603,
	sys: {
		type: 2,
		id: 2004251,
		country: 'SK',
		sunrise: 1667712542,
		sunset: 1667747288,
	},
	timezone: 3600,
	id: 724443,
	name: 'Košice',
	cod: 200,
};

export const MOCK_KOSICE_QUERY = {
	latitude: 48.71597183246423,
	longitude: 21.255670821215418,
};
