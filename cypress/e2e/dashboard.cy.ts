import {
	MOCK_BERLIN_RESPONSE,
	MOCK_KOSICE_QUERY,
	MOCK_KOSICE_RESPONSE,
	MOCK_LONDON_RESPONSE,
} from './helpers';

describe('Dashboard', () => {
	it('visits the app root url and displays default locations', () => {
		cy.intercept('/data/2.5/weather?q=London**', MOCK_LONDON_RESPONSE);
		cy.intercept('/data/2.5/weather?q=Berlin**', MOCK_BERLIN_RESPONSE);
		cy.intercept(
			`/data/2.5/weather?lat=${MOCK_KOSICE_QUERY.latitude}&lon=${MOCK_KOSICE_QUERY.longitude}**`,
			MOCK_KOSICE_RESPONSE
		);
		cy.visit('/', {
			onBeforeLoad({ navigator }) {
				// Košice city
				cy.stub(
					navigator.geolocation,
					'getCurrentPosition'
				).callsArgWith(0, { coords: MOCK_KOSICE_QUERY });
			},
		});
		cy.get('.location-card').should('have.length', 3);

		// todo assert based on position
		cy.get('.location-card').should('contain.text', 'My Location');
		cy.get('.location-card').should('contain.text', '7°C');

		cy.get('.location-card').should('contain.text', 'Berlin');
		cy.get('.location-card').should('contain.text', '12°C');

		cy.get('.location-card').last().should('contain.text', 'London');
		cy.get('.location-card').last().should('contain.text', '10°C');
	});

	// todo add tests for warning banner
	// todo add tests for adding locations
});
