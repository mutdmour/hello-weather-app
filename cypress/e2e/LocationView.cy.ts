import { MOCK_LONDON_RESPONSE } from './helpers';

describe('LocationView', () => {
	it('visits location with query', () => {
		cy.intercept('/data/2.5/weather?q=London&**', MOCK_LONDON_RESPONSE);
		cy.visit('/location?q=London');
		cy.get('.temperature-summary').should('have.length', 1);
		cy.get('.weather-summary').should('have.length', 1);
	});

	// todo add tests for lon/lan
});
